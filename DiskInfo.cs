﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Newtonsoft.Json;
namespace App
{
    public class DiskInfo
    {
        [JsonProperty()]
        List<Disk> disks = null;
        public DiskInfo()
        {
            disks = new List<Disk>();
        }
        public void StartReading() { }
        public void Read()
        {
            var tempDisks = new List<Disk>();
            foreach (var drive in DriveInfo.GetDrives())
            {
                if (!drive.IsReady || drive.DriveType != DriveType.Fixed) continue;
                tempDisks.Add(new Disk(drive.Name, drive.TotalSize, drive.TotalFreeSpace));
            }
            this.disks = tempDisks;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this.disks);
        }
    }
}