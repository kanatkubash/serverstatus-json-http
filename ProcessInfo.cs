﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Management;
using System.Threading.Tasks;
using System.Linq;
namespace App
{
    public class ProcessInfo : IDisposable
    {
        const int MSECONDS = 5000;
        int sleepTime = 0;
        [JsonProperty(ItemConverterType = typeof(DictionaryValuesConverter))]
        Dictionary<string, ProcessStat> processes = null;
        public ProcessInfo()
        {
            processes = new Dictionary<string, ProcessStat>();
        }
        public void StartReading(int sleepTime)
        {
            this.sleepTime = sleepTime;
            ManagementObjectSearcher searcher =
            new ManagementObjectSearcher(@"SELECT PercentProcessorTime,Name,TimeStamp_Sys100NS, WorkingSetPrivate 
                FROM Win32_PerfRawData_PerfProc_Process");
            var results = searcher.Get();
            foreach (ManagementObject result in results)
            {
                ProcessStat psStat = null;
                var psName = (string)result["Name"];
                if (!processes.TryGetValue(psName, out psStat))
                {
                    psStat = new ProcessStat(psName, MSECONDS / sleepTime);
                    processes[psName] = psStat;
                }
                psStat.StartReading(result);
            }
            results.Dispose();
        }
        public void Read()
        {

            var toRemove = new List<string>();
            ManagementObjectSearcher searcher =
            new ManagementObjectSearcher(@"SELECT PercentProcessorTime,Name,TimeStamp_Sys100NS, WorkingSetPrivate 
                FROM Win32_PerfRawData_PerfProc_Process");
            var results = searcher.Get();
            var currentProcesses = new List<string>();
            foreach (ManagementObject result in results)
            {
                ProcessStat psStat = null;
                var psName = (string)result["Name"];
                if (processes.TryGetValue(psName, out psStat))
                    psStat.Read(result);
                currentProcesses.Add(psName);
            }
            results.Dispose();
            foreach (var process in processes)
            {
                if (!currentProcesses.Contains(process.Key))
                    toRemove.Add(process.Key);
            }
            foreach (var key in toRemove)
            {
                processes[key].Dispose();
                processes.Remove(key);
            }
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this.processes.Values);
        }

        public void Dispose()
        {
            if (processes != null)
            {
                foreach (var process in processes)
                {
                    process.Value.Dispose();
                }
            }
        }
    }
}