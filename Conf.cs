﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
namespace App
{
    public class Conf
    {
        public int Port { get; set; }
        public string Password { get; set; }
        private Conf() { }
        public static Conf Parse(string filename)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException(filename + " not found");
            var lines = File.ReadAllLines(filename);
            var portRegex = new Regex(@"^port=(?<port>\d{1,5})\s*$");
            var passRegex = new Regex(@"^password=(?<password>.{5,})\s*$");
            int port = -1;
            string pass = null;
            foreach (var line in lines)
            {
                if (port == -1)
                {
                    var matches = portRegex.Match(line);
                    if (matches.Success) port = Convert.ToInt32(matches.Groups["port"].Value);
                }
                if (pass == null)
                {
                    var matches = passRegex.Match(line);
                    if (matches.Success) pass = matches.Groups["password"].Value;
                }
            }
            if (port == -1 || pass == null)
                throw new FormatException("File does not contain port and\\or password");
            if (port > 65535)
                throw new FormatException("Illegal port number. Should be between 1-65535");
            return new Conf() { Port = port, Password = pass };
        }
    }
}
