﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace App
{
    public class PerfCounter : IDisposable
    {
        public const int SLEEPTIME = 500;
        public static PerfCounter Instance { get; set; }
        static Thread worker;
        bool endRequested = false;
        public Data Data { get; set; }
        private PerfCounter()
        {
            Data = new Data();
        }
        public static void Initialize()
        {
            Instance = new PerfCounter();
            worker = new Thread(Instance.startUpdate) { IsBackground = true };
            worker.Start();
        }
        private void startUpdate()
        {
            while (!endRequested)
            {
                Data.CPU.StartReading();
                Data.Disks.StartReading();
                Data.Memory.StartReading();
                Data.Processes.StartReading(SLEEPTIME);
                Data.Services.StartReading();
                Thread.Sleep(SLEEPTIME);
                Data.CPU.Read();
                Data.Disks.Read();
                Data.Memory.Read();
                Data.Processes.Read();
                Data.Services.Read();
            }
        }
        public void Dispose()
        {
            endRequested = true;
        }
    }
}
