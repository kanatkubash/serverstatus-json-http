### Instructions

##### Install the certificate 
Certificate installation is necessary to access app through HTTPS.
 There's a demo self signed certificate included among with source files
>Choose local machine context
>
![Certificate store](/img/certificate.jpg)

After succesful installation, launch PowerShell and run:
```
Get-ChildItem -path cert:\LocalMachine\My
```
>Locate your certificate and copy its thumbprint
>
![Copy certificate thumbprint](/img/certhash.jpg)

Then invoke command:
```
netsh http add sslcert ipport=0.0.0.0:_port certhash=_certhash appid={_appid}
```
Where:

*	_port : listening port as defined in conf.conf file
*	_certhash : thumbprint of certificate that you have previously copied
*	_appid : application guid . You can get it from file [AssemblyInfo.cs](/Properties/AssemblyInfo.cs) . It is set to 
```
31CB98E7-C038-48CA-8CA4-C1625F8E156A
```
in this app.

Example:
```
netsh http add sslcert ipport=0.0.0.0:8080 certhash=01A451161517F1A917D887621AF15AA39FE750B0 appid={31CB98E7-C038-48CA-8CA4-C1625F8E156A}
```

##### Application HTTP commands

Send POST or GET in following format
```
http://localhost:8080?password=_password?data=_data
```
Where:

*	_data : can be one of 
	*	cpu - get CPU info
	*	memory - get memory info
	*	disk - get disk info
	*	process - get process info
	*	service - get services info
	*	all - return all above
*	_password : password set in conf.conf file