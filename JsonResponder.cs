﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace App
{
    class JsonResponder : IResponder
    {
        string password;
        Data data;
        readonly string[] DATA_TYPES = new string[] { "all", "cpu", "memory", "disk", "process", "service" };
        public string ResponseText { get; set; }
        public int StatusCode { get; set; }

        public JsonResponder(string passwordToCheck)
        {
            password = passwordToCheck;
        }

        public void SetData(Data data)
        {
            this.data = data;
        }

        public void HandleRequest(HttpListenerRequest request)
        {
            var parameters = ParseParams(request);
            if (parameters == null)
            {
                SetResponse("Incorrect request", 400);
                return;
            }
            if (parameters["password"] != password)
            {
                SetResponse("Incorrect request", 400);
                return;
            }
            SetDataResponse(parameters["data"]);
        }

        private void SetDataResponse(string dataStr)
        {
            if (dataStr == null || DATA_TYPES.Contains(dataStr))
                SetResponse("Data can be one of [all,cpu,memory,disk,service,process]", 400);
            switch (dataStr)
            {
                case "all":
                    SetResponse(data.ToString(), 200);
                    break;
                case "cpu":
                    SetResponse(data.CPU.ToString(), 200);
                    break;
                case "memory":
                    SetResponse(data.Memory.ToString(), 200);
                    break;
                case "disk":
                    SetResponse(data.Disks.ToString(), 200);
                    break;
                case "service":
                    SetResponse(data.Services.ToString(), 200);
                    break;
                case "process":
                    SetResponse(data.Processes.ToString(), 200);
                    break;
            }
        }

        private void SetResponse(string response, int code)
        {
            this.ResponseText = response;
            this.StatusCode = code;
        }

        NameValueCollection ParseParams(HttpListenerRequest request)
        {
            if (request.HttpMethod == "GET")
            {
                return request.QueryString;
            }
            else if (request.HttpMethod == "POST")
            {
                string form = "";
                using (var reader = new StreamReader(request.InputStream))
                {
                    form = reader.ReadToEnd();
                }
                if (form == null)
                    return null;
                var matches = new Regex("(?<name>[^=&]+)=(?<value>[^=&]*)").Matches(form);
                var ret = new NameValueCollection();
                foreach (Match match in matches)
                    ret.Add(match.Groups["name"].Value, match.Groups["value"].Value);
                return ret;
            }
            return null;
        }
    }
}
