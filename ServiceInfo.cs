﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ServiceProcess;
namespace App
{
    public class ServiceInfo
    {
        [JsonProperty()]
        List<Service> services = null;
        public ServiceInfo()
        {
            services = new List<App.Service>();
        }
        public void StartReading() { }
        public void Read()
        {
            var tempServices = new List<Service>();
            foreach (var service in System.ServiceProcess.ServiceController.GetServices())
            {
                tempServices.Add(new Service(service.ServiceName, service.Status));
            }
            services = tempServices;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this.services);
        }
    }
}