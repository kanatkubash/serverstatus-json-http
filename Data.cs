﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace App
{
    public class Data
    {
        [JsonProperty(propertyName: "cpu")]
        public CPUInfo CPU;
        [JsonProperty(propertyName: "memory")]
        public MemoryInfo Memory;
        [JsonProperty(propertyName: "disk")]
        public DiskInfo Disks;
        [JsonProperty(propertyName: "service")]
        public ServiceInfo Services;
        [JsonProperty(propertyName: "process")]
        public ProcessInfo Processes;
        public Data()
        {
            CPU = new CPUInfo();
            Memory = new MemoryInfo();
            Disks = new DiskInfo();
            Services = new ServiceInfo();
            Processes = new ProcessInfo();
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
