﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    public interface IResponder
    {
        /// <summary>
        /// HTTP status code after processing either GET or POST parameters
        /// </summary>
        int StatusCode { get; set; }
        /// <summary>
        /// ResponseText after processing either GET or POST parameters
        /// </summary>
        string ResponseText { get; set; }
        /// <summary>
        /// Handles either GET or POST request
        /// </summary>
        /// <param name="request">Http request to handle</param>
        void HandleRequest(HttpListenerRequest request);
    }
}
