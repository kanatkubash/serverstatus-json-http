﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace App
{
    public class Disk
    {
        const double BYTE_IN_GB = 1024 * 1024 * 1024 * 1.0;
        [JsonProperty(propertyName: "name")]
        string Name { get; set; }
        [JsonProperty(propertyName: "usedGB")]
        double Used { get; set; }
        [JsonProperty(propertyName: "freeGB")]
        double Available { get; set; }
        public Disk(string name, long total, long available)
        {
            this.Name = name;
            this.Used = Math.Round((total - available) / BYTE_IN_GB, 1);
            this.Available = Math.Round(available / BYTE_IN_GB, 1);
        }
    }
}
