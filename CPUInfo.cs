﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using System.Management;
using Microsoft.Win32;
using System.Runtime.InteropServices;
namespace App
{
    public class CPUInfo : IDisposable
    {
        [Newtonsoft.Json.JsonProperty(propertyName: "average")]
        int Average { get; set; }
        [Newtonsoft.Json.JsonProperty(propertyName: "cpus")]
        int[] CPUs { get; set; }

        PerformanceCounterCategory processors = null;
        List<PerformanceCounter> counters = null;
        public CPUInfo()
        {
            processors = new PerformanceCounterCategory("Processor");
            counters = new List<PerformanceCounter>();
            foreach (var processor in processors.GetInstanceNames())
            {
                var procTimeCounter = Array.Find(processors.GetCounters(processor),
                    counter => counter.CounterName == "% Processor Time");
                counters.Add(procTimeCounter);
            }
        }
        public void StartReading()
        {
            foreach (var counter in counters)
                counter.NextValue();
        }
        public void Read()
        {
            List<int> tempStorage = new List<int>();
            foreach (var counter in counters)
            {
                var procTime = counter.NextValue();
                if (counter.InstanceName == "_Total")
                    this.Average = (int)procTime;
                else
                {
                    tempStorage.Add((int)procTime);
                }
            }
            this.CPUs = tempStorage.ToArray();
        }
        public void ReadWmi()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_PerfFormattedData_PerfOS_Processor");
            List<int> tempStorage = new List<int>();
            foreach (var obj in searcher.Get())
            {

                var usage = obj["PercentProcessorTime"];
                var name = obj["Name"];
                if ((string)name == "_Total")
                    this.Average = (int)usage;
                else
                    tempStorage.Add((int)usage);
            }
            this.CPUs = tempStorage.ToArray();
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public void Dispose()
        {
            if (counters != null)
                counters.ForEach(counter => counter.Dispose());
        }
    }
}