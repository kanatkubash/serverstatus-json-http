﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading;
using System.IO;
using System.Net.NetworkInformation;
using System.IO.Compression;
using System.Reflection;
using System.Resources;
using System.Collections;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Web;
using System.Text.RegularExpressions;

namespace App
{
    /// <summary>
    /// Embedded web server listening on free port on localhost
    /// </summary>
    public class Server : IDisposable
    {
        #region Class variables
        HttpListener Listener { get; set; }
        public int Port { get; private set; }
        IResponder responder;
        const int MAX_THREAD = 500;
        long threadNum = 0;
        #endregion
        #region Methods
        /// <summary>
        /// Instantiates a server and starts listening to requests
        /// </summary>
        /// <param name="port">Port to listen</param>
        /// <param name="responder">IResponder instance that handles get post requests</param>
        public Server(int port, IResponder responder)
        {
            Listener = new HttpListener();
            Port = port;
            this.responder = responder;
            Listener.Prefixes.Add("https://+:" + Port + "/");
            Listener.Start();
            Console.WriteLine($"Server started at port {port}");
            _StartServer();
        }
        /// <summary>
        /// Starts server
        /// </summary>
        private void _StartServer()
        {
            new Thread(() =>
            {
                while (true)
                {
                    var ctxt = Listener.GetContext();
                    while (Interlocked.Read(ref threadNum) > MAX_THREAD)
                    {
                        Thread.Sleep(50);
                    }
                    Interlocked.Increment(ref threadNum);
                    new Thread(_HandleRequests) { IsBackground = true }.Start(ctxt);
                }
            })
            { IsBackground = true }.Start();
        }

        /// <summary>
        /// Handles requests in multithreadd manner
        /// </summary>
        /// <param name="context">Listener object to get context from</param>
        private void _HandleRequests(object objectContext)
        {
            var context = (HttpListenerContext)objectContext;
            var request = context.Request;
            Console.WriteLine("Request from " + request.RemoteEndPoint.Address);
            try
            {
                responder.HandleRequest(request);
                context.Response.Headers["Expires"] = DateTime.UtcNow.Subtract(TimeSpan.FromDays(1)).ToString("R");
                context.Response.Headers["Pragma"] = "no-cache";
                context.Response.Headers["Cache-Control"] = "no-cache, no-store, must-revalidate";
                byte[] body = Encoding.UTF8.GetBytes(responder.ResponseText);
                context.Response.ContentLength64 = body.Length;
                var streamWriter = new StreamWriter(context.Response.OutputStream);
                context.Response.StatusCode = responder.StatusCode;
                if (responder.StatusCode == 200)
                    context.Response.ContentType = "application/json";
                else
                    context.Response.ContentType = "text/plain";
                context.Response.OutputStream.Write(body, 0, body.Length);
                context.Response.OutputStream.Flush();
                context.Response.Close();
            }
            catch (HttpListenerException ht)
            {
                Console.WriteLine(ht.Message);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Unknown error: " + e.ToString());
            }
            Interlocked.Decrement(ref threadNum);
        }

        public void Dispose()
        {
            if (Listener != null && Listener.IsListening)
                Listener.Close();
        }
        #endregion
    }
}
