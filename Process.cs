﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Management;

namespace App
{
    public class ProcessStat : IDisposable
    {
        [JsonProperty(propertyName: "name")]
        string Name { get; set; }
        [JsonProperty(propertyName: "cpuAverage")]
        double CpuUsage { get; set; }
        [JsonProperty(propertyName: "memory")]
        int MemoryUsage { get; set; }
        long[] cpuSamples = null;
        int samplePointer = 0;
        ulong percentProcInitial = 0;
        ulong percentProcFinal = 0;
        ulong procTimeStampInitial = 0;
        ulong procTimeStampFinal = 0;
        public ProcessStat(string name, int samples)
        {
            Name = name;
            cpuSamples = new long[samples];
            for (var i = 0; i < samples; i++)
                cpuSamples[i] = 0;
        }
        public void StartReading(ManagementObject initial)
        {
            percentProcInitial = (ulong)initial["PercentProcessorTime"];
            procTimeStampInitial = (ulong)initial["TimeStamp_Sys100NS"];
            initial.Dispose();
        }
        public void Read(ManagementObject final)
        {
            MemoryUsage = Convert.ToInt32((ulong)final["WorkingSetPrivate"] / 1024);
            procTimeStampFinal = (ulong)final["TimeStamp_Sys100NS"];
            percentProcFinal = (ulong)final["PercentProcessorTime"];
            final.Dispose();
            var deltaProcTime = (double)(percentProcFinal - percentProcInitial);
            var deltaTime = (double)(procTimeStampFinal - procTimeStampInitial);
            cpuSamples[samplePointer] = (long)(deltaProcTime / deltaTime * 100);
            samplePointer = samplePointer == cpuSamples.Length - 1
                ? 0 : samplePointer + 1;
            CpuUsage = cpuSamples.Sum() / cpuSamples.Length / System.Environment.ProcessorCount;
        }
        public void Dispose()
        {
        }
    }
}
