﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace App
{
    public class Service
    {
        [JsonProperty(propertyName: "name")]
        string ServiceName { get; set; }
        [JsonProperty(propertyName: "state"), JsonConverter(typeof(StringEnumConverter))]
        ServiceControllerStatus State { get; set; }
        public Service(string name, ServiceControllerStatus state)
        {
            ServiceName = name;
            State = state;
        }
    }
}
