﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace App
{
    class Program
    {
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(EventHandler handler, bool add);
        private delegate bool EventHandler(CtrlType signal);
        enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }
        static EventHandler handler;
        static Server server;
        static Conf conf;
        static void Main(string[] args)
        {
            try
            {
                conf = Conf.Parse("conf.conf");
                var responder = new JsonResponder(conf.Password);
                server = new Server(conf.Port, responder);
                PerfCounter.Initialize();
                responder.SetData(PerfCounter.Instance.Data);
                handler += new EventHandler(Handler);
                SetConsoleCtrlHandler(handler, true);
                while (true) { Thread.Sleep(100); }
            }
            catch (FormatException f)
            {
                Console.WriteLine(f.Message);
            }
            catch (FileNotFoundException fn)
            {
                Console.WriteLine(fn.Message);
            }
            catch (HttpListenerException ht)
            {
                if (ht.NativeErrorCode == 5)
                    Console.WriteLine("Run as administrator");
                else if (ht.NativeErrorCode == 183)
                    Console.WriteLine("Port already in use");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        static bool Handler(CtrlType signal)
        {
            switch (signal)
            {
                case CtrlType.CTRL_C_EVENT:
                case CtrlType.CTRL_LOGOFF_EVENT:
                case CtrlType.CTRL_SHUTDOWN_EVENT:
                case CtrlType.CTRL_CLOSE_EVENT:
                default:
                    if (PerfCounter.Instance != null)
                        PerfCounter.Instance.Dispose();
                    return false;
            }
        }

    }
}
