﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Management;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic.Devices;

namespace App
{
    public class MemoryInfo : IDisposable
    {
        [JsonProperty(propertyName: "availableMb")]
        int Available { get; set; }
        [JsonProperty(propertyName: "usedMb")]
        int Used { get; set; }
        PerformanceCounterCategory memory = new PerformanceCounterCategory("Memory");
        List<PerformanceCounter> counters = null;
        public MemoryInfo()
        {
            var allCounters = memory.GetCounters();
            counters = new List<PerformanceCounter>();
            var availableCounter = Array.Find(allCounters, s => s.CounterName == "Available MBytes");
            var percentage = Array.Find(allCounters, s => s.CounterName == "% Committed Bytes In Use");
            counters.Add(availableCounter);
            //counters.Add(percentage);
        }
        public void StartReading()
        {
            foreach (var counter in counters)
                counter.NextValue();
        }
        public void Read()
        {
            double availableMb = 0, usedPercentage = 0;
            foreach (var counter in counters)
            {
                var counterValue = counter.NextValue();
                if (counter.CounterName == "Available MBytes")
                    availableMb = counterValue;
                else if (counter.CounterName == "% Committed Bytes In Use")
                {
                    usedPercentage = counterValue;
                }
            }
            this.Used = (int)(new ComputerInfo().TotalPhysicalMemory / 1024 / 1024 - availableMb);
            this.Available = (int)availableMb;
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public void Dispose()
        {
            if (counters != null)
                counters.ForEach(counter => counter.Dispose());
        }
    }
}